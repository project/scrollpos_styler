/**
 * @file
 * Request Animation Frame polyfill.
 *
 * @see https://gist.github.com/jalbam/5fe05443270fa6d8136238ec72accbc0
 */

(function () {

  'use strict';

  var vendors = ['webkit', 'moz', 'ms', 'o'],
    vp = null;

  for (var x = 0; x < vendors.length && !window.requestAnimationFrame && !window.cancelAnimationFrame; x++) {
    vp = vendors[x];
    window.requestAnimationFrame = window.requestAnimationFrame || window[vp + 'RequestAnimationFrame'];
    window.cancelAnimationFrame = window.cancelAnimationFrame || window[vp + 'CancelAnimationFrame'] || window[vp + 'CancelRequestAnimationFrame'];
  }

  // iOS6 is buggy.
  if (/iP(ad|hone|od).*OS 6/.test(window.navigator.userAgent) || !window.requestAnimationFrame || !window.cancelAnimationFrame) {
    var lastTime = 0;
    window.requestAnimationFrame = function (callback, element) {
      var now = window.performance.now();
      // First time will execute it immediately but barely noticeable and
      // performance is gained.
      var nextTime = Math.max(lastTime + 16, now);
      return setTimeout(function () {
        callback(lastTime = nextTime);
      }, nextTime - now);
    };
    window.cancelAnimationFrame = clearTimeout;
  }

}());
