<?php

namespace Drupal\scrollpos_styler\Plugin\Filter;

use Drupal\Component\Utility\Html as HtmlUtility;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a filter to attach the libraries for this module.
 *
 * @Filter(
 *   id = "scrollpos_styler",
 *   title = @Translation("Scroll Pos Styler"),
 *   description = @Translation("Adds CSS class to a HTML element depending on scroll position."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE
 * )
 */
class ScrollPosStyler extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);

    $dom = HtmlUtility::load($text);
    $xpath = new \DOMXPath($dom);
    if ($xpath->query('//*[contains(@class, "sps")]')->length) {
      $result->addAttachments([
        'library' => [
          'scrollpos_styler/library',
        ],
      ]);
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    return $this->t('
      <p>Enable ScrollPos-Styler adding (<code>class="sps"</code>) to your elements.</p>
      <p>Configure the scroll position in px to trigger the style adding (<code>data-sps-offset="1"</code>) to your elements.</p>
      <p>"sps--abv" or "sps--blw" classes will be added when the window is scrolled above/below the defined position.</p>
    ');
  }

}
